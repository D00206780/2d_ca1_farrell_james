# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* 2D Game Engine Development CA1
* 1.0

### References ###

* YouTube
	The following video helped me a lot when setting up this project - it gave me a basis to work on, and influenced my decision of making a tile-based game.
	* https://www.youtube.com/watch?v=txUvD5_ROIU

* Class
	'Snake' code that we had worked on in class also helped during the project earlier stages. The grid and cube objects were adapted from this code.

* W3Schools
	I used W3Schools when researching local storage. The following article helped me here:
	* https://www.w3schools.com/html/html5_webstorage.asp

* SnailBait 
	The following files and functions have been adapted from Snailbait code that we worked on in class:

	* Files
		* Behaviors
			* pulse.js
			* cycle.js

		* JavaScript
			* stopwatch.js 
			* sprites.js

	* Functions
		* CMYK
			* revealCountdown()
			* revealToast()
			* hideToast()
			* pollMusic()
			* restartMusic()
			* startMusic()
			* createAudioChannels()
			* getFirstAvailableAudioChannel()
			* seekAudio()
			* playAudio()
			* soundLoaded()
			* playSound()

### Who do I talk to? ###

* James Farrell (Author) - D00206780@student.dkit.ie