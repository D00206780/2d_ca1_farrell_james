class CycleBehaviour {

    constructor(duration) {
        this.cycleDuration = duration || 1000;
        this.lastCycleTime = 0;
    }

    execute(sprite, now) {

        //Time based animation
        if (now - this.lastCycleTime > this.cycleDuration) {
            this.lastCycleTime = now;
            sprite.artist.advance();
        }
    }
}