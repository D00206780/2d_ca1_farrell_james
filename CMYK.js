//This puzzle game will allow players to change the current colour of 'dynamic' blocks.
//The player can progress through any areas that is the same colour as the background.
//Static areas will not be allowed to change colour.
//Dynamic areas will change colour based on the players selection.  
//The four main colours are: Cyan, Magenta, Yellow and Black (CMYK for short).
//This is a play on the CMYK colour space.
//I originally planned to create an RGB colour space.
//However, CMYK gives me more options and overall works better.
class CMYK {

    constructor() {
        //Canvas
        this.canvas = document.getElementById('gameCanvas');
        this.context = this.canvas.getContext('2d');

        //Fps
        this.fpsElement = document.getElementById('fpsElement');

        //Toast
        this.toastElement = document.getElementById('toastElement');

        //Level
        this.lvlElement = document.getElementById('lvlElement');

        //Error
        this.errorElement = document.getElementById('errors');

        //Sound
        this.soundCheckboxElement = document.getElementById('soundCheckbox');
        this.audioSprites = document.getElementById('audioSprites');
        this.soundOn = this.soundCheckboxElement.checked;

        //Music
        this.musicCheckboxElement = document.getElementById('musicCheckbox');
        this.musicSprites = document.getElementById('musicSprites');
        this.musicOn = this.musicCheckboxElement.checked;

        this.musicSprites.volume = 0.1;

        //Image objects
        this.bgImage = new Image();
        this.logoImage = new Image();
        this.resumeImage = new Image();
        this.loadImage = new Image();
        this.creditsImage = new Image();
        this.exitImage = new Image();
        this.backImage = new Image();
        this.comingSoon = new Image();
        this.spriteSheet = new Image();

        //Game Objects
        this.map = new Map();
        this.grid = new Grid();
        this.cube = new Cube();

        //Time Objects
        this.now = new Date();

        //Image sources
        this.bgImage.src = 'images/background.png';
        this.logoImage.src = 'images/logo.png';
        this.resumeImage.src = 'images/resume.png';
        this.loadImage.src = 'images/load.png';
        this.creditsImage.src = 'images/credits.png';
        this.exitImage.src = 'images/exit.png';
        this.backImage.src = 'images/back.png';
        this.comingSoon.src = 'images/comingSoon.png';
        this.spriteSheet.src = 'images/spriteSheet.png';

        //Button data
        this.buttonX = [60, 120, 190, 280, 450];
        this.buttonY = [120, 180, 240, 300, 300];
        this.buttonWidth = [300, 300, 300, 300, 300];
        this.buttonHeight = [45, 45, 45, 45, 45];

        //Map Constants
        this.GRID_WIDTH = 20;
        this.GRID_HEIGHT = 10;
        this.TILE_WIDTH = 40;
        this.TILE_HEIGHT = 40;

        //Key Constants
        this.LEFT_ARROW = 37;
        this.RIGHT_ARROW = 39;
        this.G_KEY = 71;
        this.P_KEY = 80;
        this.Q_KEY = 81;

        //Color Enums
        this.WHITE = 0;
        this.CYAN = 1;
        this.MAGENTA = 2;
        this.YELLOW = 3;
        this.BLACK = 4;

        //Block Enums
        this.PLAYER_BLOCK = 0;
        this.EXIT_BLOCK = 5;
        this.TRANSPARANT_BLOCK = 6;

        //Menu Enums
        this.RESUME_OPTION = 0;
        this.LOAD_OPTION = 1;
        this.CREDITS_OPTION = 2;
        this.EXIT_OPTION = 3;
        this.BACK_OPTION = 4;

        //Color Enums
        this.WHITE_HEX = '#fff';
        this.CYAN_HEX = '#0ff';
        this.MAGENTA_HEX = '#f0f';
        this.YELLOW_HEX = '#ff0';
        this.BLACK_HEX = '#000';

        //Mouse tracking
        this.mousePressed = false;
        this.mousePosition = {
            x: 0,
            y: 0,
        };

        //Key Tracking
        this.keystate = {
            left: false,
            right: false,
        };

        //Game Tracking
        this.lvl = 0;
        this.paused = false;
        this.allowUpdate = true;
        this.gameStarted = false;
        this.windowHasFocus = true;
        this.countdownInProgress = false;
        this.dynamicArea = false;

        //Time Tracking
        this.sec = 0;
        this.curSec = 0;
        this.frameCount = 0;
        this.framesLastSecond = 0;
        this.moveStartTime = 0;
        this.MOVE_DURATION = 50;

        //Menu tracking
        this.inMenu = false;
        this.inMainMenu = false;

        //Mechanics
        this.gravityDirection = 1;

        //Cells
        this.CYCLE_DURATION = 125;
        this.lastCycleTime = 0;
        this.cellIndex = 0;

        //Sprites
        this.sprites = [];
        this.playerSprite;
        this.exitSprite;

        this.PULSE_DURATION = 125;
        this.OPACITY_THRESHOLD = 0.1;

        //Player Sprite Cells
        this.playerSpriteCells = [
            { top: 10, left: 10, width: 40, height: 40 },
            { top: 10, left: 56, width: 40, height: 40 },
            { top: 10, left: 103, width: 40, height: 40 },
            { top: 10, left: 150, width: 40, height: 40 },
        ];

        //Audio Location
        this.gravityFlipSound = {
            position: 0,
            duration: 100,
            volume: 0.5
        };

        this.beepSound = {
            position: 0.2,
            duration: 58,
            volume: 0.5
        };

        this.moveSound = {
            position: 0.4,
            duration: 80,
            volume: 1.0
        };

        this.smashNoise = {
            position: 0.7,
            duration: 261,
            volume: 1.0
        };

        this.clickSound = {
            position: 1.0,
            duration: 100,
            volume: 1.0
        }

        this.deathNoise = {
            position: 1.1,
            duration: 280,
            volume: 1.0
        }

        this.winNoise = {
            position: 1.3,
            duration: 400,
            volume: 1.0
        }

        //Audio Channels
        this.audioChannels = [
            { playing: false, audio: this.audioSprites, },
            { playing: false, audio: null, },
            { playing: false, audio: null, },
            { playing: false, audio: null }
        ];

        this.audioSpriteCountdown = this.audioChannels.length - 1;
    }

    //Check the storage at the start of every game 
    //Adapted snippet taken from - https://www.w3schools.com/html/html5_webstorage.asp
    checkStorage() {

        //Check browser support
        if (typeof (Storage) !== 'undefined') {

            //Check if local storage has been previously set
            if (localStorage.getItem('lvl') === null) {
                localStorage.setItem('lvl', 0);                     //Store
            } else {
                this.lvl = localStorage.getItem('lvl');             //Retreive
            }

            //Display current level
            this.lvlElement.innerHTML = this.lvl;

        } else {
            this.errorElement.innerHTML = 'Sorry, your browser does not support Web Storage...';
        }
    }

    //Update the storage at the end of every level
    updateStorage() {
        if (this.lvl >= 0) {
            localStorage.setItem('lvl', this.lvl);
            this.lvlElement.innerHTML = this.lvl;
        }
    }

    //Initialize the game
    init() {
        this.canvas.width = this.GRID_WIDTH * this.TILE_WIDTH;
        this.canvas.height = this.GRID_HEIGHT * this.TILE_HEIGHT;
        this.checkStorage();
        this.mainMenu();
    }

    mainMenu() {
        this.inMenu = true;
        this.inMainMenu = true;
        this.context.drawImage(this.bgImage, 0, 0);
        this.context.drawImage(this.logoImage, 450, 60);
        this.context.drawImage(this.resumeImage, 60, 120);
        this.context.drawImage(this.loadImage, 120, 180);
        this.context.drawImage(this.creditsImage, 190, 240);
        this.context.drawImage(this.exitImage, 280, 300);
    }

    //Display load menu
    loadMenu() {
        this.context.drawImage(this.comingSoon, 0, 0);
        this.context.drawImage(this.logoImage, 450, 60);
        this.context.drawImage(this.backImage, 450, 300);
    }

    //Display credits menu
    creditsMenu() {
        this.context.drawImage(this.comingSoon, 0, 0);
        this.context.drawImage(this.logoImage, 450, 60);
        this.context.drawImage(this.backImage, 450, 300);
    }

    //Exit the game
    exit() {
        window.close();
    }

    //Start the game
    startGame() {
        this.gameStarted = true;
        this.inMenu = false;
        this.startMusic();
        this.reset();
    }

    reset() {

        //Store direction of gravity
        this.gravityDirection = this.map.rules[this.lvl].gravityDirection;

        //Reset player position
        this.resetPlayerPosition();

        //Reset time variables
        this.resetTimeVariables();

        //Initialise the grid
        this.initialiseGrid();

        //Generate the level
        this.generateLevel();

        //Enter the game loop
        this.loop();
    }

    resetPlayerPosition() {
        this.playerSprite = null;
        this.sprites = [];
    }

    //Reset time tracking letibles to 0
    resetTimeVariables() {
        this.sec = Date.now();
        this.curSec = Date.now();
        this.frameCount = 0;
        this.framesLastSecond = 0;
        this.moveStartTime = 0;
    }

    initialiseGrid() {
        //Initialise the grid
        this.grid.init(this.GRID_WIDTH, this.GRID_HEIGHT, 0);

        //Set the grid background block color
        //By storing the first element of the current level background map 
        this.grid.setBackgroundBlockColor(this.map.getBackground(this.lvl)[0]);
    }

    //Generate a grid from our maps based on the current level
    generateLevel() {
        this.generate(this.map.getBackground(this.lvl), false);
        this.generate(this.map.getStaticArea(this.lvl), false);
        this.generate(this.map.getDynamicArea(this.lvl), true);
        this.generate(this.map.getGameObjects(this.lvl), false);
    }

    loop() {

        //Check if the game is paused
        if (!cmyk.paused) {
            cmyk.update();
            cmyk.render();
        }

        //Loop when ready
        requestAnimationFrame(cmyk.loop);
    }

    //Check for updates every loop
    update() {
        this.checkMovement();
        this.checkAdjacentBlock();
        this.checkCurrentBlockLose();
        this.checkCurrentPositionWin();
    }

    //Check if the user has requested update
    checkMovement() {

        //Return if no movement requested
        if (this.keystate.left !== true && this.keystate.right !== true) {
            return;
        }

        //Store the initial position
        let position = this.cube.get();

        //Update the x position to move left
        if (this.keystate.left === true) {
            this.keystate.left = false;
            position.x--;
        }

        //Update the x position to move right
        if (this.keystate.right === true) {
            this.keystate.right = false;
            position.x++;
        }

        //Check if the next position is out of bounds
        if (!this.checkOutOfBounds(position)) {

            //Store block colors
            let nextBlockColor = this.grid.getBlock(position);
            let backgroundBlockColor = this.grid.getBackgroundBlockColor();

            //Check if the next block is passable
            if (nextBlockColor === backgroundBlockColor || nextBlockColor === this.EXIT_BLOCK) {
                this.updatePlayerPosition(position);
                this.playSound(this.moveSound);
            }
        }
    }

    //Check the current position for a win
    checkCurrentPositionWin() {
        //Store positions
        let playerPosition = this.cube.get();
        let exitPosition = this.grid.getExitPosition();

        //Check if the players position is equal to the exit
        if (playerPosition.x == exitPosition.x && playerPosition.y == exitPosition.y) {

            //Play win sound noise
            this.playSound(this.winNoise);

            //Restart game once the player has reached the last level
            if (this.lvl < this.map.getLength() - 1) {
                this.lvl++;
            } else {
                this.lvl = 0;
            }

            //Update local storage, reset game
            this.updateStorage();
            this.reset();
        }
    }

    //Check the current block for a lose
    checkCurrentBlockLose() {
        //Store block data
        let playerBlockColor = this.grid.getBlock(this.cube.get());
        let backgroundBlockColor = this.grid.getBackgroundBlockColor();

        //Check if the player is in a non-background colored block
        //Must allow the player block to be equal to itself
        if (playerBlockColor !== backgroundBlockColor && playerBlockColor !== this.PLAYER_BLOCK) {

            //Play death sound noise
            this.playSound(this.deathNoise);

            //Restart game if the player dies on the first level
            if (this.lvl > 0) {
                this.lvl--;
            } else {
                this.lvl = 0;
            }

            //Update local storage, reset game
            this.updateStorage();
            this.reset();
        }
    }

    //Check the adjacent block for an update
    checkAdjacentBlock() {
        //Store position data
        let currentPosition = this.cube.get();

        //Store the next requested position 
        //(y + 1 to simulate gravity moving down)
        let nextPosition = {
            x: currentPosition.x,
            y: currentPosition.y + this.gravityDirection,
        }

        //Store block data
        let nextBlockColor = this.grid.getBlock(nextPosition);
        let backgroundBlockColor = this.grid.getBackgroundBlockColor();

        //If the player will be in bounds at their next requested position
        if (!this.checkOutOfBounds(nextPosition)) {

            //Check if the next requested block is empty or solid
            this.checkAdjacentBlockEmpty(nextBlockColor, backgroundBlockColor, nextPosition);
            this.checkAdjacentBlockSolid(nextBlockColor, backgroundBlockColor);
        }
    }

    //Check if the block adjacent is empty
    checkAdjacentBlockEmpty(nextBlockColor, backgroundBlockColor, nextPosition) {
        //If the next block is a background block, it must be empty
        //Must also check if the next block is an exit block
        //This allows updates to the exit block while falling
        if (nextBlockColor === backgroundBlockColor || nextBlockColor === this.EXIT_BLOCK) {
            this.updatePlayerPosition(nextPosition);
            this.allowUpdate = false;
        }
    }

    //Check if the block is solid
    checkAdjacentBlockSolid(nextBlockColor, backgroundBlockColor) {
        //If the next block isnt a background block or an exit block, it must be solid
        if (nextBlockColor !== backgroundBlockColor) {
            this.allowUpdate = true;
        }
    }

    //Update the level as the player moves through the grid
    updatePlayerPosition(nextPosition) {
        if (this.now - this.moveStartTime > this.MOVE_DURATION) {
            this.updatePlayerSpritePosition(nextPosition);
            this.updateGrid(nextPosition);
            this.moveStartTime = this.now;
        }
    }

    //update player position in the grid
    updatePlayerSpritePosition(nextPosition) {
        this.playerSprite.x = nextPosition.x * this.TILE_WIDTH;
        this.playerSprite.y = nextPosition.y * this.TILE_HEIGHT;
    }

    //Update the grid when the player moves
    updateGrid(nextPosition) {

        //Use time calculations to regulate movement speed
        if (this.now - this.moveStartTime > this.MOVE_DURATION) {

            //Store the current position
            let currentPosition = this.cube.get();

            //Store the background block color
            let backgroundBlockColor = this.grid.getBackgroundBlockColor();

            //Set the block at the current positon to the background block color
            this.grid.setBlock(currentPosition, backgroundBlockColor);

            //Set the players position
            this.cube.setPosition(nextPosition);

            //Set the block at the players position to the player block
            this.grid.setBlock(nextPosition, this.PLAYER_BLOCK);

            //Update move start time
            this.moveStartTime = this.now;
        }
    }

    //Check if the player has left the canvas bounds
    checkOutOfBounds(position) {

        //Order:
        //Out of Left Bound
        //Out of Right Bound
        //Out of Top Bound
        //Out of Bottom Bound
        //In Bounds
        if (position.x < 0) {
            position.x = this.GRID_WIDTH - 1;
        } else if (position.x > this.GRID_WIDTH - 1) {
            position.x = 0;
        } else if (position.y < 0) {
            position.y = this.GRID_HEIGHT - 1;
        } else if (position.y > this.GRID_HEIGHT - 1) {
            position.y = 0;
        } else {
            return false;
        }

        //Feedback sound
        this.playSound(this.smashNoise);

        //Update grid with new position
        this.updatePlayerPosition(position);

        return true;
    }

    //Render the game at each frame
    render() {

        //Check if the context is empty
        if (cmyk.context == null) {
            return;
        } else {

            //Update now
            this.now = Date.now();

            //Store the current full second
            this.sec = Math.floor(this.now / 1000);

            //If one full second has passed
            if (this.sec != this.curSec) {
                this.curSec = this.sec;                     //Update the current second
                this.framesLastSecond = this.frameCount;    //Store the frames in the last second
                this.frameCount = 1;                        //Reset the frame count
            } else {
                this.frameCount++;                          //Increment frames until one second has passed
            }

            //Display FPS
            this.fpsElement.innerHTML = this.framesLastSecond;

            //Draw level
            this.drawLevel();
        }
    }

    //Draw the level every frame
    drawLevel() {
        this.drawGrid();
        this.drawSprites();
    }

    //Draw each element in the grid
    drawGrid() {

        //Loop through each element in the grid
        for (let x = 0; x < this.GRID_WIDTH; x++) {
            for (let y = 0; y < this.GRID_HEIGHT; y++) {

                //Assign a color based on the current element in the grid
                switch (this.grid.get(x, y)) {
                    case this.CYAN:
                        this.context.fillStyle = this.CYAN_HEX;
                        break;
                    case this.MAGENTA:
                        this.context.fillStyle = this.MAGENTA_HEX;
                        break;
                    case this.YELLOW:
                        this.context.fillStyle = this.YELLOW_HEX;
                        break;
                    case this.BLACK:
                        this.context.fillStyle = this.BLACK_HEX;
                        break;
                }

                //Draw a colored sqaure at this position
                this.context.fillRect(x * this.TILE_WIDTH, y * this.TILE_HEIGHT, this.TILE_WIDTH, this.TILE_HEIGHT);
            }
        }
    }

    //Draw each sprites in the sprite array
    //Update each frame
    drawSprites() {
        for (let i = 0; i < this.sprites.length; ++i) {
            this.sprites[i].update(this.now);
            this.sprites[i].draw(this.context);
        }
    }

    //This function places a 1D array into a 2D space. 
    //It moves along the x axis placing blocks into the grid.
    //Once we have reached the end of the grid (i === GRID_WIDTH),
    //We can move onto the next line (y++).
    //This loops until the end of the 1D array.
    //The number 6 is used to check if a tile should not be over-written
    //The number 0 represents the playable character, and will initialise the cube object
    //
    generate(map) {
        let x = 0, y = 0;
        for (let i = 0; i < map.length; i++) {

            let thisBlock = map[i];

            //Move pointer along the grid
            if (i % this.GRID_WIDTH === 0 && i !== 0) {
                y++;
                x = 0;
            } else if (i !== 0) {
                x++;
            }

            //Store properties for creating sprites
            let properties = {
                x: x * this.TILE_WIDTH,
                y: y * this.TILE_HEIGHT,
                width: this.TILE_WIDTH,
                height: this.TILE_HEIGHT,
            }

            //Don't place transparant blocks
            if (thisBlock !== this.TRANSPARANT_BLOCK) {
                this.grid.set(x, y, map[i]);
            }
            //Initialize the player block
            if (thisBlock === this.PLAYER_BLOCK) {
                this.createPlayerSprite(properties);
                this.cube.set(x, y);
            }
            //Initialize the exit block
            if (thisBlock === this.EXIT_BLOCK) {
                this.createExitBlock(properties);
                this.grid.setExitPosition(x, y);
            }
        }
    }

    //Create exit block
    createExitBlock(properties) {
        let pulseBlock = new Block(properties, "#fff", [new PulseBehaviour(this.PULSE_DURATION, this.OPACITY_THRESHOLD)]);
        this.sprites.push(pulseBlock);
    }

    //Create player sprite
    createPlayerSprite(properties) {
        let playerSpriteArtist = new SpriteSheetArtist(this.spriteSheet, this.playerSpriteCells);
        let playerSprite = new Sprite(properties, playerSpriteArtist, [new CycleBehaviour(this.CYCLE_DURATION)]);
        this.playerSprite = playerSprite;
        this.sprites.push(playerSprite);
    }

    //Changes the direction of gravity
    flipGravity() {

        //Check if gravity is enabled for this level
        if (this.map.rules[this.lvl].flipGravity) {

            //Feedback sound
            this.playSound(this.gravityFlipSound);
            this.gravityDirection *= -1;
        }
    }

    //Change the value of the dynamic area 
    //Shift in order - C - M - Y - K
    shiftColor() {

        //Check if shift color is enabled for this level
        if (this.map.rules[this.lvl].shiftColor) {
            //Feedback sound
            this.playSound(this.beepSound);

            //Loop through dynamic area map, updating it's values
            for (let x = 0; x < this.map.getDynamicArea(this.lvl).length; x++) {
                if (this.map.getDynamicArea(this.lvl)[x] !== this.TRANSPARANT_BLOCK) {
                    this.map.getDynamicArea(this.lvl)[x]++;
                    if (this.map.getDynamicArea(this.lvl)[x] >= this.BLACK) {
                        this.map.getDynamicArea(this.lvl)[x] = this.CYAN;
                    }
                }
            }

            //Generate the grid with updated values
            this.generate(this.map.getDynamicArea(this.lvl), true);
        }
    }

    //Toggle pause state
    togglePaused() {
        if (!this.countdownInProgress) {
            if (!this.paused) {
                this.paused = true;
            } else if (!this.inMenu) {
                this.revealCountdown();
            } else {
                this.paused = false;
            }
        }
    }

    //Adapted snailbait code
    //Reveal a countdown timer whenever the game is inpaused
    revealCountdown() {

        let DIGIT_DISPLAY_DURATION = 1000;
        let originalFont = cmyk.toastElement.style.fontSize;

        cmyk.countdownInProgress = true;
        cmyk.toastElement.style.font = '72px Arial'; //Large font

        if (cmyk.windowHasFocus && cmyk.countdownInProgress) {
            cmyk.playSound(cmyk.beepSound);
            cmyk.toastElement.style.color = cmyk.CYAN_HEX; //Cyan
            cmyk.revealToast('3', 500); //Display 3 for 0.5 seconds
        }
        setTimeout(function (e) {
            if (cmyk.windowHasFocus && cmyk.countdownInProgress) {
                cmyk.playSound(cmyk.beepSound);
                cmyk.toastElement.style.color = cmyk.MAGENTA_HEX; //Magenta
                cmyk.revealToast('2', 500); //Display 2 for 0.5 seconds
            }
            setTimeout(function (e) {
                if (cmyk.windowHasFocus && cmyk.countdownInProgress) {
                    cmyk.playSound(cmyk.beepSound);
                    cmyk.toastElement.style.color = cmyk.YELLOW_HEX; //Yellow
                    cmyk.revealToast('1', 500); //Display 1 for 0.5 seconds
                }
                setTimeout(function (e) {
                    if (cmyk.windowHasFocus && cmyk.countdownInProgress) {
                        cmyk.playSound(cmyk.beepSound);
                        cmyk.toastElement.style.color = cmyk.BLACK_HEX; //Black
                        cmyk.revealToast('Go!', 500); //Display Go! for 0.5 seconds
                    }
                    setTimeout(function (e) {
                        if (cmyk.windowHasFocus && cmyk.countdownInProgress)
                            cmyk.paused = false; //Unpause the game

                        if (cmyk.windowHasFocus && cmyk.countdownInProgress)
                            cmyk.toastElement.style.fontSize = originalFont;

                        cmyk.countdownInProgress = false;

                    }, DIGIT_DISPLAY_DURATION);

                }, DIGIT_DISPLAY_DURATION);

            }, DIGIT_DISPLAY_DURATION);

        }, DIGIT_DISPLAY_DURATION);
    }

    //Adapted snailbait code
    //Reveal a toast on the screen
    revealToast(text, duration) {
        this.toastElement.innerHTML = text;
        this.toastElement.style.display = 'block';

        setTimeout(function (e) {
            cmyk.hideToast();
        }, duration);
    }

    //Adapted snailbait code
    //Hide a toast from the screen
    hideToast() {
        this.toastElement.style.display = 'none';
    }

    //Adapted snailbait code
    pollMusic() {
        let POLL_INTERVAL = 500;
        let SOUNDTRACK_LENGTH = 130;

        let timerID = setInterval(function () {
            if (cmyk.musicSprites.currentTime > SOUNDTRACK_LENGTH) {
                clearInterval(timerID);
                cmyk.restartMusic();
            }
        }, POLL_INTERVAL);
    }

    //Adapted snailbait code
    restartMusic() {
        cmyk.musicSprites.pause();
        cmyk.musicSprites.currentTime = 0;
        cmyk.startMusic();
    }

    //Adapted snailbait code
    startMusic() {
        let MUSIC_DELAY = 0;

        setTimeout(function () {
            if (cmyk.musicCheckboxElement.checked && cmyk.gameStarted) {
                cmyk.musicSprites.play();
            }
            cmyk.pollMusic();
        }, MUSIC_DELAY);
    }

    //Adapted snailbait code
    createAudioChannels() {
        for (let i = 0; i < this.audioChannels.length; ++i) {
            let channel = this.audioChannels[i];
            if (i !== 0) {
                channel.audio = document.createElement('audio');
                channel.audio.addEventListener('loadeddata', this.soundLoaded, false);
                channel.audio.src = this.audioSprites.currentSrc;
            }
            channel.audio.autobuffer = true;
        }
    }

    //Adapted snailbait code
    seekAudio(sound, audio) {
        try {
            audio.pause();
            audio.currentTime = sound.position;
        }
        catch (e) {
            if (console) {
                console.error('Cannot seek audio');
            }
        }
    }

    //Adapted snailbait code
    playAudio(audio, channel) {
        try {
            audio.play();
            channel.playing = true;
        }
        catch (e) {
            if (console) {
                console.error('Cannot play audio');
            }
        }
    }

    //Adapted snailbait code
    soundLoaded() {
        cmyk.audioSpriteCountdown--;

        if (cmyk.audioSpriteCountdown === 0) {
            if (!cmyk.gameStarted) {
                cmyk.init();
            }
        }
    }

    //Adapted snailbait code
    getFirstAvailableAudioChannel() {
        for (let i = 0; i < this.audioChannels.length; ++i) {
            if (!this.audioChannels[i].playing) {
                return this.audioChannels[i];
            }
        }
        return null;
    }

    //Adapted snailbait code
    playSound(sound) {
        if (cmyk.soundOn) {
            let channel = cmyk.getFirstAvailableAudioChannel();

            if (!channel) {
                if (console) {
                    console.warn('All audio channels are busy. Cannot play sound');
                }
            } else {
                let audio = channel.audio;
                audio.volume = sound.volume;

                cmyk.seekAudio(sound, audio);
                cmyk.playAudio(audio, channel);

                setTimeout(function () {
                    channel.playing = false;
                    cmyk.seekAudio(sound, audio);
                }, sound.duration);
            }
        }
    }

    //Process key input
    processKey(keyCode) {

        //Check if movement is allowed
        if (this.allowUpdate) {
            if (keyCode === this.LEFT_ARROW) {
                this.keystate.left = true;
            } else if (keyCode === this.RIGHT_ARROW) {
                this.keystate.right = true;
            }
        }

        //Key checks
        if (keyCode == this.P_KEY || keyCode == this.ESC_KEY) {
            this.togglePaused();
        } else if (keyCode == this.Q_KEY) {
            this.shiftColor();
        } else if (keyCode == this.G_KEY) {
            this.flipGravity();
        }
    }

    //Process mouse clicks
    processClick() {

        //Loop through each element in the button arrays
        for (let i = 0; i < this.buttonX.length; i++) {

            //If the mouse is on a button x
            if (this.mousePosition.x > this.buttonX[i] && this.mousePosition.x < this.buttonX[i] + this.buttonWidth[i]) {

                //If the mouse is on a button y
                if (this.mousePosition.y > this.buttonY[i] && this.mousePosition.y < this.buttonY[i] + this.buttonHeight[i]) {

                    //If we are currently in the main menu
                    if (this.inMainMenu) {

                        this.inMainMenu = false; //Set main menu to false as we are about to navigate elsewhere

                        //Switch based on the current index of the array
                        switch (i) {
                            case this.RESUME_OPTION:
                                this.startGame();
                                break;
                            case this.LOAD_OPTION:
                                this.loadMenu();
                                break;
                            case this.CREDITS_OPTION:
                                this.creditsMenu();
                                break;
                            case this.EXIT_OPTION:
                                this.exit();
                                break;
                            case this.BACK_OPTION:
                                this.mainMenu();
                                break;
                        }
                    } else if (i === this.BACK_OPTION) {
                        this.mainMenu();
                    }

                    //Play a click sound
                    this.playSound(this.clickSound);
                }
            }
        }
    }
};

//Launch Game
let cmyk = new CMYK();
cmyk.createAudioChannels();
cmyk.init();

//Music checkbox event listener
cmyk.musicCheckboxElement.addEventListener('change', function (e) {
    cmyk.musicOn = cmyk.musicCheckboxElement.checked;

    if (cmyk.musicOn) {
        cmyk.musicSprites.play();
    }
    else {
        cmyk.musicSprites.pause();
    }
}
);

//Sound checkbox event listener
cmyk.soundCheckboxElement.addEventListener('change', function (e) {
    cmyk.soundOn = cmyk.soundCheckboxElement.checked;
}
);

//Key press event listener
window.addEventListener('keydown', function (e) {
    cmyk.processKey(e.keyCode);
});

//Mouve move event listener
cmyk.canvas.addEventListener('mousemove', function (e) {
    cmyk.mousePosition.x = e.offsetX;
    cmyk.mousePosition.y = e.offsetY;
});

//Mouse click event listener
cmyk.canvas.addEventListener('mousedown', function () {
    cmyk.processClick();
});

//Add event listener for window blur
window.onblur = function () {
    cmyk.windowHasFocus = false;

    if (!cmyk.paused) {
        cmyk.togglePaused();
    }
};

//Add event listener for window focus
window.onfocus = function () {
    cmyk.windowHasFocus = true;

    if (cmyk.paused) {
        cmyk.togglePaused();
    }
};